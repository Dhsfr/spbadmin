<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\CustomerController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

Route::namespace('Admin')->group(function () {
    Route::group(['middleware' => 'auth', 'prefix' => 'Admin', 'as' => 'Admin.'], function () {
        Route::get('/dashboard', 'DashboardController@index')->name('dashboard');

        #User
        Route::get('/users', 'UserController@index')->name('users.index');
        Route::get('/users/add', 'UserController@create')->name('users.add');
        Route::post('/users/store', 'UserController@store')->name('users.store');
        Route::get('/users/edit/{id}', 'UserController@edit')->name('users.edit');
        Route::put('/users/update/{id}', 'UserController@update')->name('users.update');
        Route::get('/users/detail/{id}', 'UserController@show')->name('users.detail');
        Route::delete('/users/delete/{id}', 'UserController@destroy')->name('users.destroy');


        #account shopee
        Route::get('/user-shopee/add/{id}', 'UserController@create_account')->name('users_shopee.create_account');
        Route::post('/user-shopee/store', 'UserController@store_account')->name('users_shopee.store_account');
        Route::delete('/user-shopee/destroy/{id}', 'UserController@destroy_account')->name('users_shopee.destroy_account');
        Route::get('/user-shopee/edit/{id}', 'UserController@edit_account')->name('users_shopee.edit_account');
        Route::put('/user-shopee/update/{id}', 'UserController@update_account')->name('users_shopee.update_account');
    });
});

require __DIR__.'/auth.php';
