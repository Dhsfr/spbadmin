@include('admin.components.header')
@include('admin.components.navbar')
@include('admin.components.sidebar')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Detail User {{ $customer->username }}</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('Admin.dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item active"><a href="{{ route('Admin.users.index') }}">User
                                Management</a></li>
                        <li class="breadcrumb-item active">Detail User Management</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    @if (Session::has('success'))
        <div class="mx-4">
            <div class="alert alert-success">
                {{ Session::get('success') }}
                @php
                    Session::forget('success');
                @endphp
            </div>
        </div>
    @elseif(Session::has('error'))
        <div class="mx-4">
            <div class="alert alert-danger">
                {{ Session::get('error') }}
                @php
                    Session::forget('error');
                @endphp
            </div>
        </div>
    @endif
    <!-- /.content-header -->
    <div class="card mx-3">
        <div class="card-header">
            <a href="{{ route('Admin.users_shopee.create_account', $customer->user_id) }}" class="btn btn-primary"><i
                    class="fas fa-user-plus mr-1"></i> Add
                Shopee Account</a>
        </div>
        <div class="card-body px-2 pb-2">
            <section class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-hover" data-id="{{ $customer->user_id }}"
                                id="datatable_shopee_account">
                                <thead class="thead-dark">
                                    <tr>
                                        <th>#</th>
                                        <th>Shopee Account</th>
                                        <th>ACTION</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
@include('admin.components.footer')
